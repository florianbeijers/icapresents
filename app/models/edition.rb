class Edition < ActiveRecord::Base
  has_many :projects, dependent: :restrict_with_error
  validates :name, presence: true
  scope :current, -> { where(current: true) }
  scope :archived, -> { where(current: false) }
  before_save do |record|
    if record.current == true
      Edition.update_all(current: false)
    end
  end
end
