class EditionsController < ApplicationController
  respond_to :html, :json
  before_action :check_admin

  def index
    @editions = Edition.all.order(id: :desc)
    respond_with @editions
  end

  def new
    @edition = Edition.new
  end

  def edit
    @edition = Edition.where(id: params[:id]).take
  end

  def create
    @edition = Edition.new(edition_params)
    if @edition.save
      redirect_to editions_path, notice: 'Edition has been created'
    else
      render action: :new
    end
  end

  def update
    @edition = Edition.where(id: params[:id]).take
    if @edition.update(edition_params)
      redirect_to editions_path, notice: 'Edition has been updated'
    else
      render action: :edit
    end
  end

  def destroy
    @edition = Edition.where(id: params[:id]).take
    if @edition.destroy
      redirect_to editions_path, notice: 'Edition has been deleted'
    else
      redirect_to editions_path, notice: 'Edition cannot be deleted'
    end
  end

  private
    def check_admin
      unless admin?
        redirect_to root_url, notice: 'Only administrators have access.'
      end
    end

    def edition_params
      params.require(:edition).permit(:name, :current)
    end

end
