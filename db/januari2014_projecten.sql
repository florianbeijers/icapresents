--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO projects VALUES (31, 172, 'Student', 'Linke Soep', 'Tom Kamps, Marley Baidjoe, Peter Damen, Kimara Krans, Ilse Meijer, Rick Uyterwaal, Rodi Shiho', 'CICA', 'Aula', '14:00 - 16:00', 'Linke Soep is een interactieve applicatie gemaakt door 7 CICA-studenten voor de Plastic Soup Junior Foundation. Plastic afval in de oceanen is een groot probleem omdat het zich verzamelt en tot gigantische plastic soepen vormt, sommigen wel meer dan 30 keer groter dan Nederland. Onze applicatie maakt dit duidelijk aan basisschool kinderen op een leuke en speelse manier.', 'http://www.kampsmediadesign.nl/testomgeving/linkesoep/Linke_Soep_ICA_Presents.jpg', true, '2014-01-17 14:51:33.863492', '2014-01-21 14:29:27.448985');
INSERT INTO projects VALUES (32, 173, 'Student', 'NEW MUSIC VIDEO: REBEL SOLDIERS, FREEDOM FIGHTERS.', 'Jolijn Nellestein', 'DMP', 'Aula', '14:00 - 16:00', 'The new music video of the band; ''John Frost and His Souldiers''. Together with this video they release their new music album. The music video is called: ''Rebel Soldiers, Freedom Fighters'', and is produced, directed and edited by Jolijn Nellestein.', 'http://i.imgur.com/FAFl2gh.png', true, '2014-01-17 17:11:33.985525', '2014-01-18 22:27:56.910605');
INSERT INTO projects VALUES (33, 174, 'Student', 'DRAG', 'Mathijs Mooren, Chiel  van Rijn', 'DMP', 'Aula', '14:00 - 16:00', 'A short 3D animation about drag and childlabour.

The animation follows a little robot who wants to break free of his day to day drag.', 'https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-prn2/t31/1512228_478048868970111_528020852_o.jpg', true, '2014-01-17 18:35:08.564601', '2014-01-18 22:28:05.564204');
INSERT INTO projects VALUES (34, 176, 'Student', '-From Paris With Love-', 'Nieneke Elsjan', 'DMP', 'Aula', '14:00 - 16:00', 'From Paris With Love is een interactieve reisgids die getoond kan worden op verschillende platforms zoals mobiel, tablet en computer. De website laat de bijzondere romantische plekjes van Parijs zien. Van biologische duurzame restaurantjes, tot vintage-shopping. Claire en Phillip leiden je rond in deze wereld. Bon voyage!

www.frompariswithlove.nl', 'http://i.imgur.com/frWlfyh.png', true, '2014-01-20 10:09:19.399491', '2014-01-23 06:25:29.208081');
INSERT INTO projects VALUES (35, 186, 'Student', 'Neglect is Abuse', 'Jef Fleurkens', 'DMP', 'Aula', '14:00 - 16:00', 'A 3D animated short film about the neglection of kids. 
We follow the story of Isole who is neglected by his parents. ', 'http://i.imgur.com/gMyPUVS.jpg', true, '2014-01-20 14:22:02.678748', '2014-01-20 15:49:41.195583');
INSERT INTO projects VALUES (36, 168, 'Student', 'SoundShard', 'Martijn Schoenmaker, Omran Takie, William Rijksen, Bram Arts, Arjan Groenen', 'CRIA', 'Aula', '09.00 - 17.00', '"Hold on to every memory and relive them in a special way" - SoundShard

SoundShard biedt de mogelijkheid om herinneringen te verzilveren in een eigen tijdlijn. Deze herinneringen worden vastgelegd door foto''s samen te brengen met geluid. Dit geluid kan een muziek- of audio opname zijn. Je upload een aantal foto''s en geluidsfragmenten die jou dat moment laten herbeleven. Een foto zegt meer dan duizend woorden, daarnaast versterkt het geluid de herkenning van dat moment.
De applicatie maakt het mogelijk om eigen belevingen of belevingen met vrienden te delen op je eigen profiel. Het is mogelijk om audio boodschappen of muziek toe te voegen. Deze audio speelt zich af tijdens het bekijken van de foto''s. Naarmate men meer belevingen plaatst, wordt de tijdlijn steeds groter. Deze belevingen worden dan herinneringen.

It''s time to relive your memories. Let''s give it a try on http://soundshard.nl/ 
', 'https://scontent-b.xx.fbcdn.net/hphotos-prn2/t1/1517592_232191113627678_472960984_n.jpg', true, '2014-01-20 14:32:05.847', '2014-01-20 14:35:49.735921');
INSERT INTO projects VALUES (37, 188, 'Student', 'Promotievideo Alliander', 'Kyra Meijer', 'Digital Media Production', 'Aula', '14:00 - 17:00', 'De strijd om de interne IT afdeling te vermijden is duidelijk aanwezig binnen Alliander, liever doet onze business zaken met externe partijen, terwijl het intern veel goedkoper kan! Maar.. ze zijn zich helemaal niet bewust van onze aanwezigheid en wat wij voor hen kunnen betekenen, maar hoe moeten we dat nou duidelijk maken?

In de door mij gemaakte promotievideo voor Alliander leg ik uit door de relatie met de klant in beeld te brengen op welke manier wij als IT afdeling iets kunnen betekenen voor onze business. Op een speelse manier worden onze werkzaamheden aangegeven en wordt men geprikkeld om onze interne "Webdiensten website" te bekijken.

Nieuwsgierig geworden? Kom dan een kijkje nemen bij mijn maquette in de aula waar ook mijn promotievideo te bekijken valt!', 'http://imgur.com/2HEE6xm.jpg', true, '2014-01-20 15:45:20.42166', '2014-01-21 14:28:30.040982');
INSERT INTO projects VALUES (38, 189, 'Student', 'Rebel Hood', 'Kyllian, Weng Mee, Wouter, Thom, see others in description...', 'GAME', 'ICA Lounge and C012/C013', '15:15 @ C012/C013', 'Rebel Hood is a game about fairytales. Not the happy fairytales as you know from the Disney, but fairytales with a dark twist...


Once upon a time, in a world that for some only exists in fairytales, the Big Bad Wolf ruled the land with an iron claw. He treated the fairytale creatures as if they were slaves and they had to live in harsh conditions. Their ruler was merciless: killing, pillaging. He destroyed their homes and their families.

Finally, the fairytale creatures couldn’t take it anymore. They started standing up to the Big Bad Wolf. After a long and hard fight they managed to defeat the wolf and his pack. While the Big Bad Wolf begged for mercy, the fairy tale creatures, without any remorse, burned him alive. But little did they know that the wolf couldn’t be fully destroyed. Now, after ages his soul is still haunting the Dark Forbidden Forest…

Made by:
Kyllian, Weng Mee, Wouter, Thom, Evan, Yvonne, Ivo, Jan, Sebastiaan, Guoqiang, Donny, Tim, Anouk, Hugo, Maikel, Justus, Famke, Lein, Sergen, Omar, Dirk, Wesley, Justice, Bas, Stefan, Sergio, Wendy, Kevin, Andre, Douglas, Joris, Roryan, Wesley, Marc, Joep, Sven, Mitch, Orry 

', 'http://i.imgur.com/YS5zYN5.jpg', true, '2014-01-20 18:29:01.423581', '2014-01-22 22:37:24.761058');
INSERT INTO projects VALUES (39, 190, 'Student', 'Projecting', 'Lynn Gommans, Jurre Brienne, Niels ten Broeke, Zhiara Jacobs, Jorre Spijker, Marcel van Ginkel', 'CICA', 'ICA Lounge', '13:00 - 17:00', 'Projecting is interactieve projectie op een gebouw (in ons geval een maquette). Het heet ook wel ''projection mapping''. In ons prototype laten we zien hoe je interactief bezig kan zijn met film en kunst met betrekking tot het Kunstencluster.

Projecting is an interactive way of using projection mapping on a building, or in our case a scale model. Our prototype demonstrates how the user can interactively experience film and art, as an introduction to Arnhem''s future Kunstencluster.', 'http://imgur.com/N4x7UPJ.jpg', true, '2014-01-20 19:00:07.843722', '2014-01-21 14:28:04.870751');
INSERT INTO projects VALUES (40, 191, 'Student', 'Domestic Disturbance', 'Daniël Smulders', 'DMP', 'Aula', '14:00 - 16:00', 'Domestic Disturbance tells the story of Sven Masters, a 15 year old highschool student who suffers from a bipolar disorder. ', 'http://imgur.com/IXIF4q6.jpg', true, '2014-01-20 20:31:03.687593', '2014-01-21 14:27:47.885182');
INSERT INTO projects VALUES (41, 193, 'Student', 'Lusio', 'Emiel Boerefijn, Tomas Buiting, Pelle Overbeeke, Daniëlla Plomp, Niels Rovers, Jeroen Schonenberg, Damir Skenderovic, Agneta Slaman, Daan Smulders, Ramon Weijenbarg en Nicky Wiesbrock. ', 'GAME', 'ICA Lounge & C0.12', '14:30', 'Een serious game die kinderen met obesitas helpt om gezonder te leven. In opdracht van het Rijnstate Ziekenhuis.', 'http://imgur.com/TIVpinc.jpg', true, '2014-01-20 20:53:45.205683', '2014-01-21 14:27:28.187808');
INSERT INTO projects VALUES (42, 195, 'Student', 'BrandVibe', 'Remi Vledder, Martijn Brinks, Robin van Schuppen', 'ADEB', 'Aula', '14:00 - 16:00', 'De verkoop van biologische producten is al jaren aan het stijgen.
bestaan voor mens, dier en milieu staat bij steeds meer van ons
hoog in het vaandel. Toch omvat de exploitatie van biologische
producten nog maar twee procent van de markt. Er blijkt een grotere
vraag naar biologische producten te zijn, maar waarom is het
marktaandeel dan toch zo minimaal? Is het mogelijk te duur? Een veelgehoorde opmerking als het over biologisch gaat. Mogelijk dat
de biologische sector wel een duwtje in de rug kan gebruiken. Een
nieuw initiatief, genaamd BrandVibe, denkt hierin een goede partner
voor de biologische ondernemer te zijn. Maar wat biedt BrandVibe dan?', 'http://i.imgur.com/2MsEsu8.jpg?1', true, '2014-01-21 10:22:05.320995', '2014-01-22 17:31:09.754557');
INSERT INTO projects VALUES (43, 9, 'Student', 'Memory Lane', 'Casper Berlie', 'DMP', '.', '!4:00 - 17:00', 'In memory lane the viewer will be going on an abstract tour trough the city of Nijmegen. During this tour the viewer will experience all kinds of different memories.', 'https://scontent-a-ams.xx.fbcdn.net/hphotos-prn2/1513263_187266108134232_1496125659_n.png', true, '2014-01-21 11:30:14.913021', '2014-01-21 14:23:21.73048');
INSERT INTO projects VALUES (44, 199, 'Student', 'RockWith.Me', 'Bart Nijenhuis, Jordi Radstake', 'DMP', 'ICA Lounge of Aula', 'Tussen 14.00 en 16.00', 'RockWith.Me is een datingsite waarbij gebruikers gekoppeld worden d.m.v. muzieksmaak. Daarnaast wordt er een locatie gezocht die past bij de muzieksmaak zoals een danscafé in de buurt. ', 'http://imgur.com/4oGNV0o.jpg', true, '2014-01-21 11:35:28.509008', '2014-01-21 14:22:25.44934');
INSERT INTO projects VALUES (45, 200, 'Student', 'Talentjes', 'Eva Saputo, Sheena Williams, Rici Berhitu, Terry Plaate, Xander Klein Bluemink', 'PropC Project', 'ICA Lounge', '14:00 - 16:00', 'Wij Sheena Williams, Eva Saputo, Terry Plaate, Xander Klein Bluemink en Rici Berhitu vormen samen projectgroep Talentjes. Ons is gevraagd een opdracht vorm te geven voor het bedrijf iXPERIUM.  De opdracht is: een ICT-rijke oplossing ontwikkelen voor basisscholen. Onze projectgroep Talentjes is ontstaan uit de overtuiging dat elk kind een talent met zich meedraagt. Wij willen kinderen de mogelijkheid geven om er spelenderwijs achter te komen wat ze leuk vinden en waar ze goed in zijn. Talentjes wil inspelen op de creatieve en persoonlijke ontwikkeling van basisschoolkinderen. 
', 'http://imgur.com/vXhoRUW.jpg', true, '2014-01-21 12:02:51.666937', '2014-01-21 14:22:04.453019');
INSERT INTO projects VALUES (46, 140, 'Student', 'Miseria (Short Film)', 'Quita Nguyen', 'DMP', 'Aula', '14.00 - 16.00', 'The short film "Miseria" is about Lena, a confused young woman wandering around in her apartment and suffering from flashbacks and intense hallucinations. These flashbacks and hallucinations are triggered by certain objects in the house that reminds her of the past in which she celebrates their anniversary with her husband. Because of her chronic feelings of emptiness she''s having a hard time to let go of the regretting past. ', 'http://imgur.com/EHZL1Lu.jpg', true, '2014-01-21 13:28:56.307731', '2014-01-21 14:21:37.909131');
INSERT INTO projects VALUES (47, 129, 'Student', 'Astory', 'Niels Harder, Martijn Velders, Romy Gunther, Stijn Daams, Edwin van de Ridder', 'CRIA', 'ICA Lounge', '14:00 - 17:00', 'Astory is een webapplicatie waarmee de gebruiker zijn eigen interactieve verhaal kan maken. 

Wilt u een verhaal maken om bijvoorbeeld te gebruiken als storyboard, voor te lezen aan uw kinderen of te delen met uw vrienden? De krachtige editor van Astory maakt het mogelijk om uitgebreide verhalen te maken waarbij verschillende eindes en verhaallijnen mogelijk zijn. 

Zodra de gebruiker een mooi verhaal heeft gemaakt kan deze gedeeld worden zodat anderen het verhaal ook kunnen bekijken. 

Probeer het nu, vertaal je creativiteit naar een verhaal op Astory.', 'http://i.imgur.com/dFoVRw7.png', true, '2014-01-21 13:32:08.742606', '2014-01-21 14:16:55.533021');
INSERT INTO projects VALUES (48, 203, 'Student', 'Outage', 'Rik van de Kraats, Rutger Lyklema', 'DMP', 'Aula', '14:00 - 16:00', 'Voor de minor DMP (Digital Media Productions) aan de Hogeschool van Arnhem en Nijmegen werken wij, Rutger Lykelema en Rik van de Kraats, gedurende twintig weken aan het project ‘Outage’. Outage is een story exploration game die zich afspeelt op een eiland. Door het eiland te verkennen krijgt de speler meer te weten over wat er afspeelde. ', 'http://imgur.com/aUgk75G.jpg', true, '2014-01-21 16:30:27.31063', '2014-01-22 10:32:49.052121');
INSERT INTO projects VALUES (49, 169, 'Student', 'Taddle', 'Matt van Voorst, Marcel Doornbos, Kevin Atsma, Marlon Scholten', 'CRIA', 'ICA Lounge', '14:00 - 17:00', 'Iedereen zit tegenwoordig met zijn neus achter een laptop, tablet of smartphone. Van jong tot oud is technologie de grootste verslaving en vorm van vermaak geworden. Er is geen plek meer voor genegenheid, of een momentje quality time met elkaar. Dit is waar Taddle op inspeelt.

Met onze interactieve webapplicatie geven we mensen de kans om op een leuke manier tijd door te brengen met kun kinderen, met een modern sausje. In slechts drie stappen kun je al een in een spannend avontuur zitten waarin uw kind de hoofdrol speelt. Je hoeft niet in te loggen of social media accounts te koppelen. Klik op start en u kunt aan de slag, simpel!

Door kinderen de mogelijkheid te geven om hun eigen character te maken, blijft het elke keer maar weer de vraag welke held het avontuur gaat doorlopen. Avonturen die overigens door de ouders zelf bedacht kunnen worden! Met onze 3-staps storycreator kunnen ouders hun eigen scenes kiezen en zelf de tekst voor het verhaal schrijven. Zo blijft elk verhaal persoonlijk en verveeld het nooit. Geen zin om een eigen verhaal of character te maken? Dan kiest u toch één van de standaard versies!

Zo zorgt Taddle ervoor dat u en uw kind tijd samen door kunnen brengen, zelfs als ze niet achter de schermen vandaan zijn te slaan.

Taddle. Not just ''a story'', but an adventure.', 'http://imgur.com/ud8KR6y.jpg', true, '2014-01-21 23:21:26.030583', '2014-01-22 10:32:39.614834');
INSERT INTO projects VALUES (50, 201, 'Student', 'NextGen', 'Sanne Lindenschot', 'DMP', 'Aula', '14:00 - 16:00', 'Wat doe je als er bij je moeder kanker wordt geconstateerd en jij zelf draagster blijkt te zijn van het erfelijke borstkankergen BRCA1? Het overkwam Esther (41), die haar verhaal vertelt in de korte documentaire NextGen. ', 'http://imgur.com/HkUCJ7w.jpg', true, '2014-01-22 09:09:57.93169', '2014-01-22 10:32:59.011652');
INSERT INTO projects VALUES (51, 196, 'Student', 'GRIP', 'Ilonka Latorcai, Britt Kroezen', 'ADEB', 'ICA Lounge', '14.00 - 16.00 uur', 'Het bekendste rookworstwarenhuis van Nederland heeft een e-businesstraject ingevoerd om onder andere te voldoen aan wettelijke voorschriften. Wat dat betekent voor de organisatie en hoe wij dat met spelsimulatie en een interne viral gaan aanpakken, laten we zien op ICA Presents in de lounge, kom even kijken!', 'http://i.imgur.com/sOI9G9H.png', true, '2014-01-22 13:10:34.747806', '2014-01-22 16:35:20.066675');
INSERT INTO projects VALUES (52, 209, 'Student', 'REMAIN', 'Luc Vredegoor, Jeffrey Elshof, Bart Biermasz & Stefan Buiskool', 'DMP', 'Aula', '14:00 - 16:00', 'Stories make us more alive, more human, more courageous, more loving.
-Madeleine L’Engle

De kracht van beeld & geluid om verhalen over te brengen, inspireert ons om dagelijks met veel liefde bezig te zijn met ons vak.

In het kader van de Minor “Digital Media Productions” aan de hogeschool van Arnhem en Nijmegen werken vier studenten een half jaar lang aan het realiseren van een korte film.

‘REMAIN’, een visueel aantrekkelijke en artistieke film waarin er naast het bieden van entertainment, ook aangezet wordt tot denken en hierdoor een diepe indruk achterlaat bij haar publiek.

Wie zijn wij?

Even voorstellen. Wij zijn Jeffrey Elshof, Stefan Buiskool, Bart Biermasz en Luc Vredegoor, studenten aan de Hogeschool van Arnhem en Nijmegen. Tijdens onze studie Communicatie & Multimedia Design hebben wij elk diverse semesters gevolgd en hebben hierdoor een breed scala aan vakkennis en vaardigheden in huis. Tijdens de verdiepende minor “Digital Media Productions” krijgen wij de kans om een half jaar lang te werken aan een zelf opgezet project. Wij hebben er voor gekozen om een korte film te realiseren: “REMAIN”. Hierin worden film, 2D artwork en 3D animatie samen gebruikt om het verhaal te vertellen.

Bekijk onze introductiefilm 

Story

In de nabije toekomst waar technologie steeds meer invloed heeft op de samenleving wordt Aiden Seeri, een jong volwassene met grote interesse in technologie, geconfronteerd met een verleidelijke keuze. REMAIN, een ‘artificial intelligence network’ bied de mogelijkheid om uit de echte wereld te stappen en virtueel verder te leven in een perfecte wereld, geschapen naar onbewuste verlangens. Zijn besluit staat vast wanneer hij ontdekt dat de geschiedenis zich zal herhalen. Toch is er iemand die hem wilt laten zien wat er echt belangrijk is in het leven.
', 'https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-frc1/t1/1380268_397216887072090_1594389546_n.jpg', true, '2014-01-22 14:31:29.13473', '2014-01-22 15:01:08.989872');
INSERT INTO projects VALUES (53, 218, 'Student', 'Captain Jones and the adventures of Frank the turtle', 'Annelies Masselink, Jason Joannes, Floris van Raak, Aron van Engelenhoven, Han Woolderink, Eric Tuller', 'CICA', 'ICA lounge', '14:00-16:00', 'In onze oceanen en zeeën drijft steeds meer plastic afval. Door verwering, zonlicht en golfslag valt dit plastic uit elkaar in kleine stukjes. Dit leidt tot ernstige verontreiniging.

Om jonge basisschool kinderen hiervan bewust te maken hebben wij als groep een interessante interactieve applicatie gemaakt. De applicatie volgt het verhaal van Frank de schildpad die het mysterie van het plastic moet oplossen. Maar Frank kan dit niet alleen af. Wil jij hem helpen?
', 'http://imgur.com/aJlEBwj.jpg', true, '2014-01-22 20:31:04.644668', '2014-01-22 20:38:06.94743');
INSERT INTO projects VALUES (54, 221, 'Student', 'RockWith.Me', 'Jordi Radstake, Bart Nijenhuis', 'DMP', 'Aula', '14:00 - 16:00', 'RockWith.Me is daten op basis van muzieksmaak. Heb je genoeg van die eindeloze chatsessies op reguliere datingsites? Dankzij RockWith.Me ontmoet je jouw date in het echt.', 'http://i.imgur.com/RRfaC8E.png', true, '2014-01-22 22:10:18.035444', '2014-01-23 14:13:28.602403');
INSERT INTO projects VALUES (55, 231, 'Student', 'Magazijn Robot ', 'Iedereen van CAR', 'CAR (Create A Robot)', 'D0.13', '14:00 - 16:00', 'Het doel van het project is om robotjes autonoom door een magazijn te laten navigeren. Hierbij moet je je inbeelden dat de robotjes zelf een lijn volgen en de kortste route tussen A en B via C rijden. Je kunt je inbeelden dat dit de nodige complexiteit heeft, want de robotjes maken gebruik van meerdere sensoren en actuatoren om zijn gedrag te bepalen. Kom dat zien, kom dat zien!', 'http://i.imgur.com/mZIDdzu.jpg', true, '2014-01-23 09:44:34.754089', '2014-01-23 09:44:34.754089');
INSERT INTO projects VALUES (56, 187, 'Student', 'Ter Heerdt Tweewielers', 'Derrick Amsdorf', 'ADEB R', 'ICA', '14.00 -17.00', 'De MKB sector staat in deze economische tijden onder druk. Voor veel winkels is het een must om zich aan te passen aan de veranderende markt. Ook voor Ter Heerdt Tweewielers, ik onderzoek, analyseer en adviseer welke geïntegreerde IT systemen ondersteuning bieden en van toegevoegde waarde zijn voor het bedrijf. De IT systemen effectief in zetten om zo ook als bedrijf goed mee te kunnen met de veranderende markt. 
Met als resultaat meer marge, verhoging van de omzet  en borgen van continuïteit.
Doelstellingen:
•	Realiseren van een webshop met koppelingen naar leveranciers en klanten
•	Keten integratie bewerkstelligen
•	Online Strategie
', 'http://i.imgur.com/JpcTVWl.jpg', true, '2014-01-23 10:25:35.69362', '2014-01-23 10:25:35.69362');
INSERT INTO projects VALUES (57, 198, 'Student', 'DNV GL E`business oplossing High Voltage Labotory', 'Martijn Gielens, Eva Joy Palaster, Henk Mulders', 'ADEB', 'ICA Lounge', '14:00 - 16:00', 'Het onderzoeken of er door middel van een e-business tool beter aan kennisdeling kan worden gedaan. Binnen het High Voltage Laboratory van DNV GL, voorheen DNV Kema. ', 'http://i.imgur.com/PlKhPsf.jpg', true, '2014-01-23 12:20:03.218092', '2014-01-23 14:13:11.618203');
INSERT INTO projects VALUES (58, 248, 'Student', 'Digi Open Dag - Herman Brood Academie', 'Sebas Wijnen & Brett Gaasbeek', 'Digital Media Productions', 'Aula', '14:00 - 18:00', 'De Herman Brood Academie biedt een leeromgeving waarin de dynamiek van en processen in de popmuziekwereld al werkelijkheid zijn. Centraal staat de muziek die in verschillende contexten wordt gemaakt. Om van die muziek daadwerkelijk een product te maken, vervullen de studenten van alle opleidingen diverse functies, zoals dat in de professionele wereld gebruikelijk is. Daarbinnen werken de studenten van de verschillende opleidingen structureel samen.

Om potentiële studenten een duidelijk beeld te geven van de opleidingen die de HBA aanbiedt; hebben wij een aantal video''s gecreëerd. Deze video''s vertellen hoe elke opleiding in elkaar zit, zodat de kijker een duidelijk beeld krijgt van de opleidingen die de HBA aanbiedt. 
', 'http://www.hermanbroodacademie.nl/UserFiles/test/HBA131_de_opleiding.jpg', true, '2014-01-23 12:51:16.469476', '2014-01-23 14:13:03.908842');
INSERT INTO projects VALUES (59, 249, 'Student', 'The Circle', 'Daan Gerrits', 'DMP', 'Aula', '14:00 - 16:00', 'Een korte film over een jongen die last krijgt van psychose, waarin hij meegesleurd wordt door zijn psycholoog die twee persoonlijkheden heeft. Ook komt hij achter de geheime affaire van de psycholoog met zijn vriendin. 

De vraag is: Is dit echt, of speelt alles zich in zijn hoofd af?', 'https://scontent-a.xx.fbcdn.net/hphotos-frc3/t31/1506369_467535750017319_1387820070_o.jpg', true, '2014-01-23 12:55:03.799905', '2014-01-23 14:12:11.493272');
INSERT INTO projects VALUES (60, 252, 'Student', 'LNAGRO', 'Pim Teunissen, Patrick Woord', 'ADEB-DT', 'ICA Lounge', '14:00', 'Advies- en implementatieplan voor de verkoop van industriële
infraroodverwarmingspanelen voor LNAGRO ‘’ de Ondernemerij ’’', 'http://imgur.com/CnQThyj.jpg', true, '2014-01-23 12:58:22.860968', '2014-01-23 14:12:03.147535');
INSERT INTO projects VALUES (61, 361, 'Student', 'Trouble at the Ranch', 'Niels Janssen', 'GAME', 'ICA Lounge', '14:00 - 17:00', 'Trouble at the Ranch is a game about a grumpy old farmer named George. George thinks he can talk with animals. 
On his farm he has everything his little heart desires; a beautiful stretch of land, his lovely wife "Violet" and his best buddy "Betsy", a domesticated chicken. One day his wife has disappeared mysteriously and he has no clue where to look so he goes on a quest to find her back with the help of all the animals on his farm, but will they all co-operate?', 'http://troubleattheranch.com/images/slideshow/slide2.png', true, '2014-01-23 15:55:45', '2014-01-23 15:55:46');


--
-- PostgreSQL database dump complete
--

