class AddEditionReferenceToProject < ActiveRecord::Migration
  def change
    add_reference :projects, :edition, index: true
  end
end
