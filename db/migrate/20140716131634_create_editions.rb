class CreateEditions < ActiveRecord::Migration
  def change
    create_table :editions do |t|
      t.string :name
      t.boolean :current, null: false, default: false
    end
  end
end
