class AddArchivedEditionsData < ActiveRecord::Migration
  def change
    require 'rubyXL'

    connection = ActiveRecord::Base.connection

    ## Editions

    Edition.delete_all

    Edition.create(id: 1, name: 'juli 2013', current: false)
    Edition.create(id: 2, name: 'januari 2014', current: false)

    last_edition_id = 3
    Edition.create(id: last_edition_id, name: 'juni 2014', current: true)

    # sequence has to be set explicitly
    connection.execute("SELECT setval('editions_id_seq', #{last_edition_id})");

    ## Projects

    # run only once when edition_id isn't set yet
    if Project.where(edition_id: last_edition_id).count == 0
      # existing projects belong to current (last) edition
      Project.update_all(edition_id: last_edition_id)
    end

    # Projects - juli 2013

    Project.delete_all(['edition_id = ?', 1])

    workbook = RubyXL::Parser.parse(File.dirname(__FILE__) + '/../juli2013_projecten.xlsx')
    worksheet = workbook.worksheets[0]
    rows = worksheet.get_table()[:table]
    puts "Inserting #{rows.count} records into projects"

    # if sequences are not reseted in production database we can fill in the id gaps
    rows.each_with_index do |row, index|
      Project.create!(
        id: index+1,
        title: row['naam'],
        students: row['studenten'],
        semester: row['semester'],
        location: '-',
        time: '-',
        description: row['beschrijving'],
        picture: row['afbeelding'],
        approved: true,
        edition_id: 1
      )
    end

    # Projects - januari 2014

    Project.delete_all(['edition_id = ?', 2])

    File.open(File.dirname(__FILE__) + '/../januari2014_projecten.sql') do |f|
      connection.execute(f.read)
    end

    Project.where(edition_id: nil).update_all(edition_id: 2)

  end
end
